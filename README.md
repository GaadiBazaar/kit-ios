# IXKit

[![CI Status](https://img.shields.io/travis/rajkumarp-at-75733951934/IXKit.svg?style=flat)](https://travis-ci.org/rajkumarp-at-75733951934/IXKit)
[![Version](https://img.shields.io/cocoapods/v/IXKit.svg?style=flat)](https://cocoapods.org/pods/IXKit)
[![License](https://img.shields.io/cocoapods/l/IXKit.svg?style=flat)](https://cocoapods.org/pods/IXKit)
[![Platform](https://img.shields.io/cocoapods/p/IXKit.svg?style=flat)](https://cocoapods.org/pods/IXKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

IXKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'IXKit'
```

## Author

rajkumarp-at-75733951934, fcw3fdF6RNNCQS63c/irmlPp/jSdmPTDcBp7dvNTNrQ=

## License

IXKit is available under the MIT license. See the LICENSE file for more info.
